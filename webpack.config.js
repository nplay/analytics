
module.exports = {
    mode: 'development',
    entry: {
        bis2bis: './src/platform/bis2bis/index.js',
        signativa: './src/platform/signativa/index.js'
    },
    output: {
        path: `/enviroment/www/cdn/vitrine/1.0.0/analytics/`,
        filename: (chunkData) => {
            return '[name].js'
        }
    }
};