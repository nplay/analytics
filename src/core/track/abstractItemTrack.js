
import HistoryAPI from './API/historyAPI'

class AbstractItemTrack
{
    constructor()
    {
        this._historyAPI = new HistoryAPI()
    }

    /**
     * Visualized item
     */
    async item()
    {
        //Retrive current item
        let analytics = window.vitrineApp.clients.get(token).publicLibraries.analytics
        let item = analytics.pageInformation.item()

        if(item == null)
            return null
        
        let sessionID = await window.vitrineApp.clients.get(token).user.sessionID

        let data = {
            prospectItens: [],
            itens: [{
                originCollection: [],
                product: item
            }]
        }

        await this._historyAPI.insert(sessionID, data)
    }

    /**
     * Clicked item in vitrine
     */
    async prospectItem(data)
    {
        let sessionID = await window.vitrineApp.clients.get(token).user.sessionID
        
        let body = {
            itens: [],
            prospectItens: data
        }

        await this._historyAPI.insert(sessionID, body)
    }

    /**
     * Success send order
     */
    async order()
    {
        throw new Error(`Please implement method in platform librarie`)
    }
}

export default AbstractItemTrack