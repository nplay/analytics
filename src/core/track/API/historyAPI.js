
class HistoryAPI
{
    constructor()
    {
        this.URL = `${window.vitrineApp.SERVICE.history.insert.URL}`
    }

    async insert(sessionID, data)
    {
        let body = Object.assign({
            sessionID: sessionID,
        }, data)

        let RequestClass = window.vitrineApp.publicClasses.loader.RequestHelper
        let objRequest = new RequestClass(this.URL, {
            'Authorization-token': token
        })

        let response = await objRequest.post(body)
        sessionID = response.data._id

        return true
    }
}

export default HistoryAPI