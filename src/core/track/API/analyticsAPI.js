
class AnalyticsAPI
{
    constructor()
    {
        this.URL = `${window.vitrineApp.SERVICE.api.analytics.insert.URL}`
    }

    async insert(body)
    {
        let RequestClass = window.vitrineApp.publicClasses.loader.RequestHelper
        let objRequest = new RequestClass(this.URL, {
            'Authorization-token': token
        })

        let response = await objRequest.post(body)
        return true
    }
}

export default AnalyticsAPI