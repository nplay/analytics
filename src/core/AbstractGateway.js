
import AbstractPageInformation from './pageInformation/index'
import AbstractEvent from './event/abstractEvent';
import AbstractItemTrack from './track/abstractItemTrack';

class AbstractGateway
{
    constructor()
    {
        this.pageInformation = new AbstractPageInformation
        this.event = new AbstractEvent
        this.track = new AbstractItemTrack
    }

    initializeMethods()
    {
        this.track.item()
        this.track.order()
    }
}

export default AbstractGateway