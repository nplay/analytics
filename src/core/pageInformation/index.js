
class AbstractPageInformation
{
    get PAGE_TYPE_PRODUCT()
    {
        return 'product'
    }

    category()
    {
        return window.vitrineMeta.pageCategoryID
    }

    page()
    {
       return window.vitrineMeta.pageType
    }

    item()
    {
        if(this.page() != this.PAGE_TYPE_PRODUCT)
            return null;
        
        let products = this.itens()
        return products[0] 
    }

    itens()
    {
        return window.vitrineMeta.products
    }

    isMobile()
    {
        let navigator = window.navigator

        if(!navigator)
            return null

        return navigator.userAgent.indexOf('Mobile') > -1
    }
}

export default AbstractPageInformation