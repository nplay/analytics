import ExitWindowAction from "./action/exitWindowAction"
import EnterWindowAction from "./action/enterWindowAction";
import BuyAction from './action/buyAction'

class AbstractEvent
{
    constructor()
    {
        /**
         * Dispatch event's
         */
        this.targetEvt = new EventTarget()

        /**
         * Save events listeners
         */
        this.listeners = new Object()

        /**
         * action: 'exit_window', 'enter_window', 'buy_item',
         * page: null, 'cart', 'home', 'category',
         * additionalInformation eg: [{itemID: 921}]
         * window.vitrineApp.clients.get(token).publicLibraries.analytics.event.targetEvt.addEventListener('all', (bla) => {console.log(bla, 321); })
         */
        this._loadActions()
    }

    addListener(type, method)
    {
        this.listeners[type] = method
        return this.targetEvt.addEventListener(type, this.listeners[type]);
    }

    removeListener(type)
    {
        debugger
        let listener = this.listeners[type]
        return this.targetEvt.removeEventListener(type, listener)
    }

    _loadActions()
    {
        new ExitWindowAction(this.targetEvt).run()
        new EnterWindowAction(this.targetEvt).run()
        new BuyAction(this.targetEvt).run()
    }
}

export default AbstractEvent