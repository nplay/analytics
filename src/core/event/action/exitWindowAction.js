import AbstractAction from "./abstractAction";

class ExitWindowAction extends AbstractAction
{
   constructor(targetEvent)
   {
      super(targetEvent, 'all')
   }

   setAction(){
      return super.setAction('exit_window');
   }

   run()
   {
      this.rootElement.addEventListener('mouseleave', (evt) => {
         this.process()
         super.dispatch()
      })
   }
}

export default ExitWindowAction