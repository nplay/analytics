import AbstractAction from "./abstractAction";

class BuyAction extends AbstractAction
{
   constructor(targetEvent)
   {
      super(targetEvent, 'all')
   }

   setAction(){
      return super.setAction('buy_item');
   }

   setAdditionalInformation(){
      this.additionalInformation = new Array
      
      this.additionalInformation.push({
         itemID: this.itemID
      })

      return this
   }

   run()
   {
      let elements = document.querySelectorAll('.bt-add-cart')

      for(let ele of elements)
      {
         if(!ele.dataset.id)
            continue;

         ele.addEventListener('click', () => {
            this.itemID = ele.dataset.id

            if(!this.itemID)
               return;
            
            this.process()
            super.dispatch()
         })
      }
   }
}

export default BuyAction