
class AbstractAction
{
   constructor(targetEvent, eventName)
   {
      this.targetEvent = targetEvent
      this.event = null;
      
      this.eventName = eventName
      this.action = null
      this.page = null
      this.additionalInformation = null

      /**
      * eventName: 'all', 'specific_evt'
      * action: 'exit_window', 'enter_window', 'buy_item',
      * page: null, 'cart', 'home', 'category',
      * additionalInformation eg: [{itemID: 921}]
      */
   }

   setAction(val){
      this.action = val || null
      return this
   }

   /**
    * Retrive root element of page
    */
   get rootElement(){
      let element = window.document
      return element
   }

   setPage(){
      let curPage = window.vitrineApp.clients.get(token).publicLibraries.analytics.pageInformation.page() || null
      this.page = curPage
      return this
   }

   setAdditionalInformation(val){
      this.additionalInformation = val || new Array
      return this
   }

   process()
   {
      this.setPage()
      this.setAction()
      this.setAdditionalInformation()
   }

   dispatch()
   {
      let customEvt = this.getCustomEvent()
      return this.targetEvent.dispatchEvent(customEvt)
   }

   getCustomEvent()
   {
      return new CustomEvent(this.eventName, {
         detail: {
            action: this.action,
            page: this.page,
            additionalInformation: this.additionalInformation
         }
      });
   }

   run()
   {
      this.process()
      super.dispatch()
   }
}

export default AbstractAction