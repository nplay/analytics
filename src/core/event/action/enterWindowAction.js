import AbstractAction from "./abstractAction";

class EnterWindowAction extends AbstractAction
{
   constructor(targetEvent)
   {
      super(targetEvent, 'all')
   }

   setAction(){
      return super.setAction('enter_window');
   }

   run()
   {
      this.rootElement.addEventListener('mouseenter', () => {
         this.process()
         super.dispatch()
      })
   }
}

export default EnterWindowAction