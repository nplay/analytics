
import AbstractGateway from '../../core/AbstractGateway'
import PageInformation from './pageInformation/index'
import ItemTrack from './track/itemTrack';

class Analytics extends AbstractGateway
{
    constructor()
    {
        super()

        this.pageInformation = new PageInformation
        this.track = new ItemTrack
    }
}
window.vitrineApp.clients.get(token).publicLibraries.analytics = new Analytics
window.vitrineApp.clients.get(token).publicLibraries.analytics.initializeMethods()