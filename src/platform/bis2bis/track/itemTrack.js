import AbstractItemTrack from "../../../core/track/abstractItemTrack";
import AnalyticsAPI from "../../../core/track/API/analyticsAPI"


class ItemTrack extends AbstractItemTrack
{
    constructor()
    {
       super()
    }

    async order()
    {
        let layer = window.vitrineMeta

        let transactionObject = {
            "identification": null,
            "items": new Array,
            "quantity": 0,
            "subTotal": 0,
            "total": 0,
            "sessionID": await window.vitrineApp.clients.get(token).user.sessionID
        }

        let pageType = window.vitrineApp.clients.get(token).publicLibraries.analytics.pageInformation.page()
        
        if(pageType != 'transaction')
            return false;

        transactionObject.identification = layer.transaction.id
            
        for(let product of layer.transaction.itens)
        {
            let itemTransaction = {
                id: product.id,
                entityId: product.entityId,
                name: product.name,
                quantity: parseFloat(product.quantity),
                price: parseFloat(product.price)
            }

            transactionObject.quantity += product.quantity
            transactionObject.items.push(itemTransaction)
        }

        transactionObject.subTotal = layer.transaction.total - layer.transaction.tax
        transactionObject.total = layer.transaction.total

        let analyticsAPI = new AnalyticsAPI()
        return await analyticsAPI.insert(transactionObject)
    }
}

export default ItemTrack