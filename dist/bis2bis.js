/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/platform/bis2bis/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/core/AbstractGateway.js":
/*!*************************************!*\
  !*** ./src/core/AbstractGateway.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _pageInformation_index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./pageInformation/index */ \"./src/core/pageInformation/index.js\");\n/* harmony import */ var _event_abstractEvent__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./event/abstractEvent */ \"./src/core/event/abstractEvent.js\");\n/* harmony import */ var _track_abstractItemTrack__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./track/abstractItemTrack */ \"./src/core/track/abstractItemTrack.js\");\n/* harmony import */ var _helper_cookieHelper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./helper/cookieHelper */ \"./src/core/helper/cookieHelper.js\");\n\n\n\n\n\n\nclass AbstractGateway\n{\n    constructor()\n    {\n        this.pageInformation = new _pageInformation_index__WEBPACK_IMPORTED_MODULE_0__[\"default\"]\n        this.event = new _event_abstractEvent__WEBPACK_IMPORTED_MODULE_1__[\"default\"]\n        this.track = new _track_abstractItemTrack__WEBPACK_IMPORTED_MODULE_2__[\"default\"]\n    }\n\n    initializeMethods()\n    {\n        window.vitrineApp.user.sessionID = new _helper_cookieHelper__WEBPACK_IMPORTED_MODULE_3__[\"default\"]().getCookie('user.sessionID')\n        this.track.item()\n        this.track.order()\n    }\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (AbstractGateway);\n\n//# sourceURL=webpack:///./src/core/AbstractGateway.js?");

/***/ }),

/***/ "./src/core/event/abstractEvent.js":
/*!*****************************************!*\
  !*** ./src/core/event/abstractEvent.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _action_exitWindowAction__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./action/exitWindowAction */ \"./src/core/event/action/exitWindowAction.js\");\n/* harmony import */ var _action_enterWindowAction__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./action/enterWindowAction */ \"./src/core/event/action/enterWindowAction.js\");\n/* harmony import */ var _action_buyAction__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./action/buyAction */ \"./src/core/event/action/buyAction.js\");\n\n\n\n\nclass AbstractEvent\n{\n    constructor()\n    {\n        /**\n         * Dispatch event's\n         */\n        this.targetEvt = new EventTarget()\n\n        /**\n         * action: 'exit_window', 'enter_window', 'buy_item',\n         * page: null, 'cart', 'home', 'category',\n         * additionalInformation eg: [{itemID: 921}]\n         * window.vitrineApp.publicLibraries.analytics.event.targetEvt.addEventListener('all', (bla) => {console.log(bla, 321); })\n         */\n\n        this._loadActions()\n    }\n\n    addListener(type, method)\n    {\n        return this.targetEvt.addEventListener(type, method);\n    }\n\n    _loadActions()\n    {\n        new _action_exitWindowAction__WEBPACK_IMPORTED_MODULE_0__[\"default\"](this.targetEvt).run()\n        new _action_enterWindowAction__WEBPACK_IMPORTED_MODULE_1__[\"default\"](this.targetEvt).run()\n        new _action_buyAction__WEBPACK_IMPORTED_MODULE_2__[\"default\"](this.targetEvt).run()\n    }\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (AbstractEvent);\n\n//# sourceURL=webpack:///./src/core/event/abstractEvent.js?");

/***/ }),

/***/ "./src/core/event/action/abstractAction.js":
/*!*************************************************!*\
  !*** ./src/core/event/action/abstractAction.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n\nclass AbstractAction\n{\n   constructor(targetEvent, eventName)\n   {\n      this.targetEvent = targetEvent\n      this.event = null;\n      \n      this.eventName = eventName\n      this.action = null\n      this.page = null\n      this.additionalInformation = null\n\n      /**\n      * eventName: 'all', 'specific_evt'\n      * action: 'exit_window', 'enter_window', 'buy_item',\n      * page: null, 'cart', 'home', 'category',\n      * additionalInformation eg: [{itemID: 921}]\n      */\n   }\n\n   setAction(val){\n      this.action = val || null\n      return this\n   }\n\n   /**\n    * Retrive root element of page\n    */\n   get rootElement(){\n      let element = window.document\n      return element\n   }\n\n   setPage(){\n      let curPage = window.vitrineApp.publicLibraries.analytics.pageInformation.page() || null\n      this.page = curPage\n      return this\n   }\n\n   setAdditionalInformation(val){\n      this.additionalInformation = val || new Array\n      return this\n   }\n\n   process()\n   {\n      this.setPage()\n      this.setAction()\n      this.setAdditionalInformation()\n   }\n\n   dispatch()\n   {\n      let customEvt = this.getCustomEvent()\n      return this.targetEvent.dispatchEvent(customEvt)\n   }\n\n   getCustomEvent()\n   {\n      return new CustomEvent(this.eventName, {\n         detail: {\n            action: this.action,\n            page: this.page,\n            additionalInformation: this.additionalInformation\n         }\n      });\n   }\n\n   run()\n   {\n      this.process()\n      super.dispatch()\n   }\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (AbstractAction);\n\n//# sourceURL=webpack:///./src/core/event/action/abstractAction.js?");

/***/ }),

/***/ "./src/core/event/action/buyAction.js":
/*!********************************************!*\
  !*** ./src/core/event/action/buyAction.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _abstractAction__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./abstractAction */ \"./src/core/event/action/abstractAction.js\");\n\n\nclass BuyAction extends _abstractAction__WEBPACK_IMPORTED_MODULE_0__[\"default\"]\n{\n   constructor(targetEvent)\n   {\n      super(targetEvent, 'all')\n   }\n\n   setAction(){\n      return super.setAction('buy_item');\n   }\n\n   setAdditionalInformation(){\n      this.additionalInformation = new Array\n      \n      this.additionalInformation.push({\n         itemID: this.itemID\n      })\n\n      return this\n   }\n\n   run()\n   {\n      let elements = document.querySelectorAll('.bt-add-cart')\n\n      for(let ele of elements)\n      {\n         if(!ele.dataset.id)\n            continue;\n\n         ele.addEventListener('click', () => {\n            this.itemID = ele.dataset.id\n\n            if(!this.itemID)\n               return;\n            \n            this.process()\n            super.dispatch()\n         })\n      }\n   }\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (BuyAction);\n\n//# sourceURL=webpack:///./src/core/event/action/buyAction.js?");

/***/ }),

/***/ "./src/core/event/action/enterWindowAction.js":
/*!****************************************************!*\
  !*** ./src/core/event/action/enterWindowAction.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _abstractAction__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./abstractAction */ \"./src/core/event/action/abstractAction.js\");\n\n\nclass EnterWindowAction extends _abstractAction__WEBPACK_IMPORTED_MODULE_0__[\"default\"]\n{\n   constructor(targetEvent)\n   {\n      super(targetEvent, 'all')\n   }\n\n   setAction(){\n      return super.setAction('enter_window');\n   }\n\n   run()\n   {\n      this.rootElement.addEventListener('mouseenter', () => {\n         this.process()\n         super.dispatch()\n      })\n   }\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (EnterWindowAction);\n\n//# sourceURL=webpack:///./src/core/event/action/enterWindowAction.js?");

/***/ }),

/***/ "./src/core/event/action/exitWindowAction.js":
/*!***************************************************!*\
  !*** ./src/core/event/action/exitWindowAction.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _abstractAction__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./abstractAction */ \"./src/core/event/action/abstractAction.js\");\n\n\nclass ExitWindowAction extends _abstractAction__WEBPACK_IMPORTED_MODULE_0__[\"default\"]\n{\n   constructor(targetEvent)\n   {\n      super(targetEvent, 'all')\n   }\n\n   setAction(){\n      return super.setAction('exit_window');\n   }\n\n   run()\n   {\n      this.rootElement.addEventListener('mouseleave', (evt) => {\n         this.process()\n         super.dispatch()\n      })\n   }\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (ExitWindowAction);\n\n//# sourceURL=webpack:///./src/core/event/action/exitWindowAction.js?");

/***/ }),

/***/ "./src/core/helper/cookieHelper.js":
/*!*****************************************!*\
  !*** ./src/core/helper/cookieHelper.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n\nclass CookieHelper\n{\n  constructor(sufixExtra)\n  {\n    this._sufixCookie = 'vitrineApp_'\n\n    if(sufixExtra != undefined)\n      this._sufixCookie += sufixExtra\n  }\n\n  setCookie(cname, cvalue, exdays)\n  {\n    exdays = exdays == undefined ? 30 : exdays\n\n    var d = new Date();\n    d.setTime(d.getTime() + (exdays*24*60*60*1000));\n    var expires = \"expires=\"+ d.toUTCString();\n    document.cookie = this._sufixCookie+cname + \"=\" + cvalue + \";\" + expires + \";path=/\";\n  }\n\n  getCookie(cname)\n  {\n      var name = this._sufixCookie+cname + \"=\";\n      var decodedCookie = decodeURIComponent(document.cookie);\n      var ca = decodedCookie.split(';');\n      for(var i = 0; i <ca.length; i++) {\n        var c = ca[i];\n        while (c.charAt(0) == ' ') {\n          c = c.substring(1);\n        }\n        if (c.indexOf(name) == 0) {\n          return c.substring(name.length, c.length);\n        }\n      }\n      return null\n  }\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (CookieHelper);\n\n//# sourceURL=webpack:///./src/core/helper/cookieHelper.js?");

/***/ }),

/***/ "./src/core/pageInformation/index.js":
/*!*******************************************!*\
  !*** ./src/core/pageInformation/index.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n\nclass AbstractPageInformation\n{\n    category()\n    {\n        return this._findDataLayerProperty('pageCategoryID')\n    }\n\n    page()\n    {\n        let result = this._findDataLayerProperty('google_tag_params', (layer) => {\n            return layer.hasOwnProperty('google_tag_params') &&\n               layer.google_tag_params.hasOwnProperty('ecomm_pagetype')\n        })\n\n        return result.ecomm_pagetype\n    }\n\n    _findDataLayerProperty(property, customFilter)\n    {\n        let dataLayer = window.dataLayer || []\n\n        customFilter = customFilter || function(layer){\n            let objLayer = new Object(layer)\n            return objLayer.hasOwnProperty(property)\n        }\n\n        let result = dataLayer.find(layer => {\n            if(typeof layer != 'object')\n                console.warn(`Datalayer is property no object: ${layer}`)\n\n            let exists = customFilter(layer)\n            return exists\n        })\n\n        if(result == undefined)\n            return null\n        \n        return result[property]\n    }\n\n    item()\n    {\n        let result = this._findDataLayerProperty('ecommerce', (layer) => {\n            return layer.hasOwnProperty('ecommerce') &&\n                    layer.ecommerce.hasOwnProperty('detail')\n        })\n\n        if(!result)\n            return null;\n        \n        let products = result.detail.products\n\n        if(products.length > 1)\n            console.warn('Analytics detected many once item in current Page')\n        \n        return products[0]\n    }\n\n    itens()\n    {\n        let result = this._findDataLayerProperty('ecommerce', (layer) => {\n            return layer.hasOwnProperty('ecommerce') &&\n                    layer.ecommerce.hasOwnProperty('impressions')\n        })\n\n        if(!result)\n            return null;\n        \n        let products = result.impressions.filter(impression => {\n            return impression != null\n        })\n        \n        return products\n    }\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (AbstractPageInformation);\n\n//# sourceURL=webpack:///./src/core/pageInformation/index.js?");

/***/ }),

/***/ "./src/core/track/API/analyticsAPI.js":
/*!********************************************!*\
  !*** ./src/core/track/API/analyticsAPI.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n\nclass AnalyticsAPI\n{\n    constructor()\n    {\n        this.URL = `${window.vitrineApp.API.URL}${window.vitrineApp.API.analytics.insert.URL}`\n    }\n\n    async insert(body)\n    {\n        let RequestClass = window.vitrineApp.publicClasses.loader.RequestHelper\n        let objRequest = new RequestClass(this.URL)\n\n        let response = await objRequest.post(body)\n        console.log(response)\n        return true\n    }\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (AnalyticsAPI);\n\n//# sourceURL=webpack:///./src/core/track/API/analyticsAPI.js?");

/***/ }),

/***/ "./src/core/track/API/historyAPI.js":
/*!******************************************!*\
  !*** ./src/core/track/API/historyAPI.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _helper_cookieHelper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../helper/cookieHelper */ \"./src/core/helper/cookieHelper.js\");\n\n\nclass HistoryAPI\n{\n    constructor()\n    {\n        this.URL = `${window.vitrineApp.API.URL}${window.vitrineApp.API.history.insert.URL}`\n    }\n\n    async insert(sessionID, ecommerce)\n    {\n        let body = Object.assign({\n            sessionId: sessionID,\n        }, ecommerce)\n\n        let RequestClass = window.vitrineApp.publicClasses.loader.RequestHelper\n        let objRequest = new RequestClass(this.URL)\n\n        let response = await objRequest.post(body)\n        sessionID = response.data.data._id\n\n        window.vitrineApp.user.sessionID = sessionID\n\n        new _helper_cookieHelper__WEBPACK_IMPORTED_MODULE_0__[\"default\"]().setCookie('user.sessionID', sessionID)\n        return true\n    }\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (HistoryAPI);\n\n//# sourceURL=webpack:///./src/core/track/API/historyAPI.js?");

/***/ }),

/***/ "./src/core/track/abstractItemTrack.js":
/*!*********************************************!*\
  !*** ./src/core/track/abstractItemTrack.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _API_historyAPI__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./API/historyAPI */ \"./src/core/track/API/historyAPI.js\");\n\n\n\nclass AbstractItemTrack\n{\n    constructor()\n    {\n        this._historyAPI = new _API_historyAPI__WEBPACK_IMPORTED_MODULE_0__[\"default\"]()\n    }\n\n    /**\n     * Visualized item\n     */\n    async item()\n    {\n        //Retrive current item\n        let analytics = window.vitrineApp.publicLibraries.analytics\n        let item = analytics.pageInformation.item()\n\n        if(item == null)\n            return null\n        \n        let sessionID = window.vitrineApp.user.sessionID\n        let ecommerceData = {\n            ecommerce: {\n                impressions: new Array(item)\n            },\n            click: {\n                products: new Array()\n            }\n        }\n        \n        await this._historyAPI.insert(sessionID, ecommerceData)\n    }\n\n    /**\n     * Clicked item in vitrine\n     */\n    async prospectItem(itemID, lineID, description)\n    {\n        let sessionID = window.vitrineApp.user.sessionID\n        let ecommerceData = {\n            ecommerce: {\n                impressions: new Array()\n            },\n            click: {\n                products: new Array({id: itemID, lineID: lineID, description: description})\n            }\n        }\n        \n        await this._historyAPI.insert(sessionID, ecommerceData)\n    }\n\n    /**\n     * Success send order\n     */\n    async order()\n    {\n        throw new Error(`Please implement method in platform librarie`)\n    }\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (AbstractItemTrack);\n\n//# sourceURL=webpack:///./src/core/track/abstractItemTrack.js?");

/***/ }),

/***/ "./src/platform/bis2bis/index.js":
/*!***************************************!*\
  !*** ./src/platform/bis2bis/index.js ***!
  \***************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _core_AbstractGateway__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../core/AbstractGateway */ \"./src/core/AbstractGateway.js\");\n/* harmony import */ var _pageInformation_index__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./pageInformation/index */ \"./src/platform/bis2bis/pageInformation/index.js\");\n/* harmony import */ var _track_itemTrack__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./track/itemTrack */ \"./src/platform/bis2bis/track/itemTrack.js\");\n\n\n\n\n\nclass Analytics extends _core_AbstractGateway__WEBPACK_IMPORTED_MODULE_0__[\"default\"]\n{\n    constructor()\n    {\n        super()\n\n        this.pageInformation = new _pageInformation_index__WEBPACK_IMPORTED_MODULE_1__[\"default\"]\n        this.track = new _track_itemTrack__WEBPACK_IMPORTED_MODULE_2__[\"default\"]\n    }\n}\n\nwindow.vitrineApp.publicLibraries.analytics = new Analytics()\nwindow.vitrineApp.publicLibraries.analytics.initializeMethods()\n\n//# sourceURL=webpack:///./src/platform/bis2bis/index.js?");

/***/ }),

/***/ "./src/platform/bis2bis/pageInformation/index.js":
/*!*******************************************************!*\
  !*** ./src/platform/bis2bis/pageInformation/index.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _core_pageInformation__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../core/pageInformation */ \"./src/core/pageInformation/index.js\");\n\n\nclass PageInformation extends _core_pageInformation__WEBPACK_IMPORTED_MODULE_0__[\"default\"]\n{\n    constructor()\n    {\n        super()\n    }\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (PageInformation);\n\n//# sourceURL=webpack:///./src/platform/bis2bis/pageInformation/index.js?");

/***/ }),

/***/ "./src/platform/bis2bis/track/itemTrack.js":
/*!*************************************************!*\
  !*** ./src/platform/bis2bis/track/itemTrack.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _core_track_abstractItemTrack__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../core/track/abstractItemTrack */ \"./src/core/track/abstractItemTrack.js\");\n/* harmony import */ var _core_track_API_analyticsAPI__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../core/track/API/analyticsAPI */ \"./src/core/track/API/analyticsAPI.js\");\n\n\n\n\nclass ItemTrack extends _core_track_abstractItemTrack__WEBPACK_IMPORTED_MODULE_0__[\"default\"]\n{\n    constructor()\n    {\n       super()\n    }\n\n    async order()\n    {\n        let dataLayer = window.dataLayer\n        let transactionObject = {\n            \"identification\": null,\n            \"items\": new Array,\n            \"quantity\": 0,\n            \"subTotal\": 0,\n            \"total\": 0,\n            \"sessionId\": window.vitrineApp.user.sessionID\n        }\n\n        let layer = dataLayer.find(curLayer => {\n            return curLayer.hasOwnProperty('event') && curLayer.event == 'transactionSuccess'\n        })\n\n        if(layer == null)\n            return false;\n\n        transactionObject.identification = layer.transactionId\n            \n        for(let product of layer.transactionProducts)\n        {\n            let itemTransaction = {\n                id: product.sku,\n                name: product.name,\n                quantity: product.quantity,\n                price: product.price,\n                product_type: null, //Category automatically in back-end\n                cod_origin: 1 //1 Vitrine | 2 Platform\n            }\n\n            transactionObject.quantity += product.quantity\n            transactionObject.items.push(itemTransaction)\n        }\n\n        transactionObject.subTotal = layer.transactionTotal - (layer.transactionTax + layer.transactionShipping)\n        transactionObject.total = layer.transactionTotal\n\n        let analyticsAPI = new _core_track_API_analyticsAPI__WEBPACK_IMPORTED_MODULE_1__[\"default\"]()\n        return await analyticsAPI.insert(transactionObject)\n    }\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (ItemTrack);\n\n//# sourceURL=webpack:///./src/platform/bis2bis/track/itemTrack.js?");

/***/ })

/******/ });